<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;


class Mascota extends Model
{
	use HasRelationships;

    public function clientes()
    {
        $clienteModel = Voyager::modelClass('Cliente');

        return $this->belongsToMany($clienteModel, 'cliente_mascotas')
                    ->select(app($clienteModel)->getTable().'.*')
                    ->union($this->hasMany($clienteModel))->getQuery();
    }
}