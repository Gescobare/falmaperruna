<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;


class Tipodocumento extends Model
{
	use HasRelationships;

    public function tipodocumentos()
    {
        return $this->belongsTo(Cliente::class)
        ->orderBy('Descripcion', 'ASC');
    }
}

