<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
	use HasRelationships;
    use SoftDeletes;
	
	protected $table = 'clientes';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    protected $fillable = ['identidad', 'nombres', 'apellidos', 'telefono', 'direccion', 'correo', 'nacimiento', 'genero', 'image'];

	public $additional_attributes = ['full_name'];

    public function getFullNameAttribute()
	{
    	return "{$this->nombres} {$this->apellidos}";
	}

}
