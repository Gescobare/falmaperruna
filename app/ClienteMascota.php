<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClienteMascota extends Model
{
    public function idCliente()
    {
        return $this->belongsTo(Cliente::class)
        ->orderBy('Name', 'ASC');
    }

    public function idMascota()
    {
        return $this->belongsTo(Mascota::class)
        ->orderBy('Name', 'ASC');
    }
}
