<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;


class Registropago extends Model
{
	use HasRelationships;

    public function idCliente()
    {
        return $this->belongsTo(Cliente::class)
        	->orderBy('nombres', 'DESC');
    }
}
