<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;


class City extends Model
{
    use HasRelationships;

    public function idCountry()
    {
        return $this->belongsTo(Country::class)
        ->orderBy('Name', 'ASC');
    }
}
