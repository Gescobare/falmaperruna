<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;


class Country extends Model
{
	use HasRelationships;
	
	protected $table = 'countries';

	protected $primaryKey = "Code";
	
	protected $hidden = [
        
    ];

	protected $guarded = [];


    public function countries()
    {
        return $this->belongsTo(Cliente::class)
        ->orderBy('Name', 'DESC');
    }
}
