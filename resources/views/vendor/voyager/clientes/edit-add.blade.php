@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <style>
        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height:100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.clientes.update', $dataTypeContent->id) }}@else{{ route('voyager.clientes.store') }}@endif" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel panel-bordered panel-dark">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="voyager-people"></i> Datos del cliente
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                             <div class="form-group">
                                <label for="documento">Nº Documento</label>
                                <input type="text" class="form-control" id="identidad" name="identidad" placeholder="Nº Documento" value="@if(isset($dataTypeContent->identidad)){{ $dataTypeContent->identidad }}@endif" required="">
                            </div>
                            <div class="form-group">
                                <label for="id_tipodocumento">Documento</label>
                                <select class="form-control" name="id_tipodocumento" required="">
                                    <option value="">Ninguno</option>
                                    @foreach(App\Tipodocumento::all() as $tipodocumento)
                                        <option value="{{ $tipodocumento->id }}"@if(isset($dataTypeContent->id_tipodocumento) && $dataTypeContent->id_tipodocumento == $tipodocumento->id) selected="selected"@endif>{{ $tipodocumento->descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>                            
                            <div class="form-group">
                                <label for="nombres">Nombres</label>
                                <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres" value="@if(isset($dataTypeContent->nombres)){{ $dataTypeContent->nombres }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="apellidos">Apellidos</label>
                                <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" value="@if(isset($dataTypeContent->apellidos)){{ $dataTypeContent->apellidos }}@endif">
                            </div>
                            <div class="form-group">                                        
                                <label for="name">Género</label>
                                <ul class="radio">
                                    <li>
                                        <input type="radio" id="option-genero-masculino" name="genero" value="Masculino">
                                        <label for="option-genero-masculino">Masculino</label>
                                        <div class="check"></div>
                                    </li>
                                    <li>
                                        <input type="radio" id="option-genero-femenino" name="genero" value="Femenino" checked="">
                                        <label for="option-genero-femenino">Femenino</label>
                                        <div class="check"></div>
                                    </li>
                                    <li>
                                        <input type="radio" id="option-genero-otro" name="genero" value="Otro">
                                        <label for="option-genero-otro">Otro</label>
                                        <div class="check"></div>
                                    </li>
                                </ul>
                            </div>                                                        
                        </div>
                    </div>
                    <div class="panel panel panel-bordered panel-dark">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="voyager-book"></i> Datos Adicionales</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="nacimiento">Cumpleaños</label>
                                <input type="date" class="form-control" id="nacimiento" name="nacimiento" placeholder="Cumpleaños" value="@if(isset($dataTypeContent->nacimiento)){{ $dataTypeContent->nacimiento }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="correo">Correo</label>
                                <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" value="@if(isset($dataTypeContent->correo)){{ $dataTypeContent->correo }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="telefono">Teléfono</label>
                                <input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="@if(isset($dataTypeContent->telefono)){{ $dataTypeContent->telefono }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="direccion">Dirección</label>
                                <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="@if(isset($dataTypeContent->direccion)){{ $dataTypeContent->direccion }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="id_pais">País</label>
                                <select class="form-control select2 select2-hidden-accessible" name="id_pais">
                                    <option value="">Ninguno</option>
                                    @foreach(App\Country::all() as $country)
                                        <option value="{{ $country->Code }}"@if(isset($dataTypeContent->id_pais) && $dataTypeContent->id_pais == $country->Code) selected="selected"@endif>{{ $country->Name }}</option>
                                    @endforeach
                                </select>
                            </div>                          
                        </div>
                    </div><!-- .panel -->                    
                </div>
                <div class="col-md-4">
                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i>Imagen</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(isset($dataTypeContent->image))
                                <img src="{{ filter_var($dataTypeContent->image, FILTER_VALIDATE_URL) ? $dataTypeContent->image : Voyager::image( $dataTypeContent->image ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image">
                        </div>
                    </div>
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i>Mascotas / Apadrinados</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <!--<div class="form-group">
                                <label for="id_mascota">Mascota</label>
                                <select class="form-control" name="id_mascota">
                                    @foreach(App\Mascota::all() as $mascota)
                                        <option value="{{ $mascota->id }}"@if(isset($dataTypeContent->id_mascota) && $dataTypeContent->id_mascota == $mascota->id) selected="selected"@endif>{{ $mascota->nombre }}</option>
                                    @endforeach
                                </select>
                            </div> -->
                            <!-- <div class="form-group">
                                <label for="id_categoriamascota">Categoría Mascota</label>
                                <select class="form-control" name="id_categoriamascota" required="">
                                    <option value="">Ninguno</option>
                                    @foreach(App\Categoriamascota::all() as $catmascota)
                                        <option value="{{ $catmascota->id }}"@if(isset($dataTypeContent->id_catmascota) && $dataTypeContent->id_categoriamascota == $catmascota->id) selected="selected"@endif>{{ $catmascota->descripcion }}</option>
                                    @endforeach
                                </select>
                            </div> -->
                            <div class="form-group">
                                <label for="mascota_id">Mascota</label>
                                <select class="form-control select2 select2-hidden-accessible" name="cliente_belongstomany_mascota_relationship[]" multiple="" tabindex="-1" aria-hidden="true">
                                    <option value="">Ninguno</option>
                                    @foreach(App\Mascota::all() as $mascota)
                                        <option value="{{ $mascota->id }}"@if(isset($dataTypeContent->id_mascota) && $dataTypeContent->id_mascota == $mascota->id) selected="selected"@endif>{{ $mascota->nombre }}</option>
                                    @endforeach
                                </select>       
                            </div>
                        </div>                
                    </div>
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="voyager-thumbs-up"></i> Redes Sociales</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="facebook"><i class="voyager-facebook"></i> | Facebook</label>
                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="URL Facebook" value="@if(isset($dataTypeContent->facebook)){{ $dataTypeContent->facebook }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="instagram"><i class="voyager-camera"></i> | Instagram</label>
                                <input type="text" class="form-control" id="instagram" name="instagram" placeholder="URL Instagram" value="@if(isset($dataTypeContent->instagram)){{ $dataTypeContent->instagram }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="twitter"><i class="voyager-twitter"></i> | Twitter</label>
                                <input type="text" class="form-control" id="twitter" name="twitter" placeholder="URL Twitter" value="@if(isset($dataTypeContent->twitter)){{ $dataTypeContent->twitter }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="otra"><i class="voyager-medal-rank-star"></i> | Otra</label>
                                <input type="text" class="form-control" id="otrared" name="otrared" placeholder="URL" value="@if(isset($dataTypeContent->otrared)){{ $dataTypeContent->otrared }}@endif">
                            </div>                            
                        </div>
                    </div><!-- .panel -->
                </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>

@stop
